import React, { createContext, useContext, useEffect, useState } from 'react';
import { calculateNewScores, getRandom } from 'Utils';
import { CatService } from 'Services';

const AppContext = createContext<AppContext>({
  loaded: false,
  currentMash: {} as Mash,
  loadCats: () => [],
  newMash: () => {},
  voteForLeft: () => {},
  voteForRight: () => {},
});

interface Props {
  children: React.ReactNode;
}

export const AppProvider: React.FC<Props> = ({ children }) => {
  const [loaded, setLoaded] = useState(false);
  const [currentMash, setCurrentMash] = useState<Mash>({} as Mash);

  const loadCats = (): Cat[] => CatService.fetchCats();

  const newMash = () => {
    const cats = loadCats();
    const lastIndex = cats.length - 1;
    const indexOffset = 1 + getRandom(7);

    const leftIndex = getRandom(lastIndex);

    // choose a random index around left index so that we have cats of roughly the same level
    const rightIndex = leftIndex === lastIndex ? leftIndex - indexOffset : Math.min(leftIndex + indexOffset, lastIndex);

    setCurrentMash({
      left: cats[leftIndex],
      right: cats[rightIndex],
    });
  };

  const updateScores = (winner: Cat) => {
    if (!currentMash) return;

    const loser = currentMash.left.catId !== winner.catId ? currentMash.left : currentMash.right;

    const [winnerScore, loserScore] = calculateNewScores(winner.score, loser.score);

    CatService.updateCat(winner.catId, winnerScore);
    CatService.updateCat(loser.catId, loserScore);
  };

  const voteFor = (cat: Cat) => {
    setLoaded(false);
    updateScores(cat);
    newMash();

    // mock call timing of a real api to be able to see the loader
    setTimeout(() => setLoaded(true), 500);
  };

  const voteForLeft = () => voteFor(currentMash.left);

  const voteForRight = () => voteFor(currentMash.right);

  // init first mash
  useEffect(() => {
    newMash();
    setLoaded(true);
  }, []);

  const value: AppContext = {
    loaded,
    currentMash,
    loadCats,
    newMash,
    voteForLeft,
    voteForRight,
  };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};

export const useApp = (): AppContext => useContext(AppContext);
