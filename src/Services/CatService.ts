import cats from 'Assets/cats.json';

const CATS_LS_KEY = 'catmash-cats';

const initCats = (): Cat[] => {
  const initialCats = cats.map(
    (url, index): Cat => ({
      catId: index,
      score: 1000,
      imageUrl: url,
    }),
  );

  saveCats(initialCats);

  return initialCats;
};

const fetchCats = (): Cat[] => {
  const catsJson = localStorage.getItem(CATS_LS_KEY);

  return catsJson ? JSON.parse(catsJson) : initCats();
};

const saveCats = (cats: Cat[]): void => {
  cats.sort((cat1, cat2) => cat2.score - cat1.score);
  localStorage.setItem(CATS_LS_KEY, JSON.stringify(cats));
};

const updateCat = (catId: number, score: number): void => {
  const cats = fetchCats();

  const cat = cats.find(cat => cat.catId === catId);
  if (!cat) return;

  cat.score = score;

  saveCats([...cats.filter(cat => cat.catId !== catId), cat]);
};

export const CatService = {
  fetchCats,
  updateCat,
};
