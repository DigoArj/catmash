import React, { useState } from 'react';
import { Loader } from 'Components';

interface Props {
  srcUrl: string;
  alt: string;
  score?: number;
  small?: boolean;
  rank?: number;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

export const Cat: React.FC<Props> = ({ srcUrl, alt, small = false, score, rank, onClick }) => {
  const [loaded, setLoaded] = useState(false);

  const handleOnLoad = () => setLoaded(true);

  return (
    <div
      className={`${small ? 'h-36 w-36' : 'h-48 w-48 md:h-64 md:w-64'}
                  group hover:scale-110 cursor-pointer relative overflow-hidden rounded-3xl box-border border-4 border-solid border-gray-400 bg-gray-400 shadow-lg transition-transform duration-500 ease-[cubic-bezier(0.68,-0.6,0.32,1.6)]`}
      onClick={onClick}
    >
      {!loaded && <Loader className="max-h-full max-w-[50%] m-auto" />}

      <img
        src={srcUrl}
        alt={alt}
        onLoad={handleOnLoad}
        className="h-full w-full object-cover object-center select-none transition-opacity duration-500"
        style={{ opacity: loaded ? '1' : '0' }}
      />

      {rank && (
        <span className="select-none font-serif font-bold text-6xl absolute left-2 bottom-2 bg-clip-text text-transparent bg-[#485461] bg-[linear-gradient(315deg,#485461,#28313b)] to-75% [-webkit-text-stroke-width:2px] [-webkit-text-stroke-color:white]">
          {rank}
        </span>
      )}

      {score !== undefined && (
        <div className="group-hover:opacity-100 opacity-0 transition-opacity ease-in-out duration-300 select-none absolute top-2 right-2 text-black bg-gray-400 p-1 rounded-xl text-xl">
          {score}
        </div>
      )}
    </div>
  );
};
