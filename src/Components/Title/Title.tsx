import React from 'react';

export const Title = () => (
  <header className="flex flex-col items-center justify-center select-none my-8">
    <h1 className="text-4xl md:text-8xl font-display text-white">catmash</h1>
    <h2 className="text-xl md:text-4xl font-sans text-white">Lequel sera le plus choupinou ?</h2>
  </header>
);
