import React from 'react';
import { Loader, Scores, Title, Vote } from 'Components';
import { useLocation } from 'react-router-dom';
import { useApp } from 'Stores';

export const App: React.FC = () => {
  const { pathname } = useLocation();
  const { loaded } = useApp();

  return (
    <div className="w-screen h-screen flex flex-col bg-[#200122] bg-[linear-gradient(105deg,#6f0000,#200122)]">
      <div className="w-screen md:w-[1280px] h-full mx-auto my-0 flex flex-col">
        <Title />

        <main className="flex items-center justify-evenly flex-col md:flex-row">{loaded ? <Vote /> : <Loader white />}</main>

        <Scores isOpen={pathname === '/scores'} />
      </div>
    </div>
  );
};
