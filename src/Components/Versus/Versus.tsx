import React from 'react';

export const Versus: React.FC = () => (
  <div className="font-marker text-white text-5xl select-none my-4">
    <span className="relative -top-1">V</span>
    <span className="relative -bottom-1">S</span>
  </div>
);
