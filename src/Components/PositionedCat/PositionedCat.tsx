import React from 'react';
import { Cat } from 'Components';

interface Props {
  id: number;
  imgUrl: string;
  position: number;
  score?: number;
}

export const PositionedCat: React.FC<Props> = ({ id, imgUrl, score, position }) => {
  const positionColors = `
    ${position === 1 && 'from-gold-start to-gold-end'}
    ${position === 2 && 'from-silver-start to-silver-end'}
    ${position === 3 && 'from-bronze-start to-bronze-end'}
  `;

  return (
    <li className="flex">
      <span
        className={`font-serif font-bold text-[250px] leading-[250px] select-none bg-clip-text text-transparent relative -right-10 bg-gradient-to-br ${positionColors} to-75%`}
      >
        {position}
      </span>

      <Cat srcUrl={imgUrl} alt={`cat ${id}`} score={score} />
    </li>
  );
};
