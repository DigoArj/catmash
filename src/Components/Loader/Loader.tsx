import React from 'react';
import Lottie from 'lottie-react';
import animationData from 'Assets/cats-loader.json';
import animationDataWhite from 'Assets/cats-loader-white.json';

interface Props {
  white?: boolean;
  className?: string;
}

export const Loader: React.FC<Props> = ({ white = false, className }) => (
  <Lottie animationData={white ? animationDataWhite : animationData} className={`h-auto w-1/2 md:h-[300px] md:w-[300px] ${className}`} />
);
