/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        'gold-start': '#fec84e',
        'gold-end': '#ffdea8',
        'silver-start': '#f5f7fa',
        'silver-end': '#b8c6db',
        'bronze-start': '#f2c17d',
        'bronze-end': '#b82e1f',
      },
    },
    fontFamily: {
      sans: ['Itim', 'sans-serif'],
      display: ['Trade Winds', 'cursive'],
      marker: ['Permanent Marker', 'cursive'],
      serif: ['Noticia Text', 'serif'],
    },
  },
  plugins: [],
};
